# >> kir68k :: portconf
~ `/etc/portage` configuration used by my machines ~

## >> Info
Git branches in this repository are based on the hostname of a machine, e.g. if the hostname is `server`, the branch will inherit said hostname.

Current branches:
- `relativity` (stream)
    - Profile: `llvm-systemd-desktop-gnome` from xira <sub>[tea](https:///tea.krxt.dev/kir68k/xira) | [ice](https://codeberg.org/kir68k/xira)</sub>
- `mobility`
    - Profile: `llvm-systemd-desktop-gnome` from xira <sub>[tea](https:///tea.krxt.dev/kir68k/xira) | [ice](https://codeberg.org/kir68k/xira)</sub>
